<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Orders extends CI_Controller
{
    function  __construct() {
        parent::__construct();
        
    }
     
    function index(){
        $data = array();
        $this->template->set('title', 'Orders');
        $this->template->load('template', 'contents' , 'orders', $data);
    }
     
   
}
