<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories extends CI_Controller
{
    function  __construct() {
        parent::__construct();
        
    }
     
    function index(){
        $data = array();
        $this->template->set('title', 'Categories');
        $this->template->load('template', 'contents' , 'categories', $data);
    }
     
   
}
