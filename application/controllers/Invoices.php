<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Invoices extends CI_Controller
{
    function  __construct() {
        parent::__construct();
        
    }
     
    function index(){
        $data = array();
        $this->template->set('title', 'Invoices');
        $this->template->load('template', 'contents' , 'invoices', $data);
    }
     
   
}
