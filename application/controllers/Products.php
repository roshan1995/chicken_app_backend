<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends CI_Controller
{
    function  __construct() {
        parent::__construct();
        
    }
     
    function index(){
        $data = array();
        $this->template->set('title', 'Products');
        $this->template->load('template', 'contents' , 'products', $data);
    }
     
   
}
