<html>
<head>
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="products">Products</a></li>
      <li><a href="categories">Category</a></li>
      <li><a href="orders">Orders</a></li>
      <li><a href="invoices">Invoices</a></li>
    </ul>
  </div>
</nav>
<div style="padding-top:45px;height:540px">

    <div id="contents"><?php echo $contents ?></div>	
</div>	
</body>
</html>
