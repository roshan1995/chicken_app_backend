<div class="container">
  <form class="form-horizontal" action="/action_page.php">

	<div class="col-sm-6">
	    <div class="form-group">
	      <label class="control-label col-sm-2" for="cat_name">Category Name:</label>
	      <div class="col-sm-10">
	        <input type="text" class="form-control" id="cat_name" placeholder="Enter Category Name" name="cat_name">
	      </div>
	    </div>
	    <div class="form-group">
	      <label class="control-label col-sm-2" for="cat_slug">Category Slug:</label>
	      <div class="col-sm-10">          
	        <input type="text" class="form-control" id="cat_slug" placeholder="Enter Category Slug" name="cat_slug">
	      </div>
	    </div>
	</div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>